const Vec3 = require('vec3').Vec3
const logger = require('../utils/logger')

let bot, stringTo, findItemType, isWindowOpened, inventory

function init(_bot, _stringTo, _findItemType, _isWindowOpened, _inventory) {
  bot = _bot
  stringTo = _stringTo
  findItemType = _findItemType
  isWindowOpened = _isWindowOpened
  inventory = _inventory
}

function listInventory(done) {
  const output = inventory.myItems().map(a => a[0] + ':' + a[1])
    .join(', ')
  if (output) { logger.info(output) } else { logger.info('empty inventory') }
  done()
}

function toss(number, itemName, done) {
  const item = findItemType(itemName)
  if (item) {
    bot.toss(item.id, null, number, () => {
      done()
    })
  } else {
    logger.info(`I have no ${ itemName }`)// change this maybe
    done()
  }
}

function equip(destination, item, done) {
  if (item != null) {
    bot.equip(item, destination, err => {
      if (err) {
        setTimeout(() => done(`unable to equip ${ item.name }. ${ err }`), 200)
      } else {
        logger.info(`equipped ${ item.name }`)
        setTimeout(done, 200)
      }
    })
  } else {
    done(`I have no such item.`)
  }
}

function unequip(destination, done) {
  bot.unequip(destination)
  done()
}

function findCraftingTable() {
  const cursor = new Vec3()
  for (cursor.x = bot.entity.position.x - 4; cursor.x < bot.entity.position.x + 4; cursor.x++) {
    for (cursor.y = bot.entity.position.y - 4; cursor.y < bot.entity.position.y + 4; cursor.y++) {
      for (cursor.z = bot.entity.position.z - 4; cursor.z < bot.entity.position.z + 4; cursor.z++) {
        const block = bot.blockAt(cursor)
        if (block.type === 58) return block
      }
    }
  }
}

function craft(amount, name, done) {
  const item = findItemType(name)
  const craftingTable = findCraftingTable()
  const wbText = craftingTable ? 'with a crafting table, ' : 'without a crafting table, '
  if (item == null) {
    done(`${ wbText } unknown item: ${ name }`)
  } else {
    const recipes = bot.recipesAll(item.id, null, craftingTable)
    if (recipes.length) {
      logger.info(`${ wbText } I can make ${ item.name }`)
      const numberOfOperation = Math.ceil(amount / recipes[0].result.count)
      const newAmount = numberOfOperation * recipes[0].result.count
      bot.craft(recipes[0], numberOfOperation, craftingTable, err => {
        if (err) {
          logger.error(`Cannot make ${ amount } ${ name }: ${ err }`)
          done()
        } else {
          logger.info(`I made ${ newAmount } ${ item.name }`)
          done()
        }
      })
    } else {
      done(`${ wbText } I can't make ${ item.name }`)
    }
  }
}

function activateItem(done) {
  bot.activateItem()
  done()
}

function deactivateItem(done) {
  bot.deactivateItem()
  done()
}

function click(slot, done) {
  bot.clickWindow(slot, 0, 0)
  done()
}

function tool(slot, done) {
  bot.setQuickBarSlot(slot)
  done()
}

function closeWindow(done) {
  if (isWindowOpened()) { bot.closeWindow(bot.currentWindow) }
  done()
}

module.exports = {
  listInventory,
  toss,
  equip,
  unequip,
  craft,
  activateItem,
  deactivateItem,
  init,
  tool,
  click,
  closeWindow
}
