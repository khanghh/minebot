const logger = require('../utils/logger')
// block types allowed to be used as scaffolding
const scaffoldBlockTypes = {
  1: true, // stone
  2: true, // grass block
  3: true, // dirt
  4: true, // cobblestone
  87: true // netherrack
}

let materials
let bot
const assert = require('assert')

function init(_bot, _materials) {
  bot = _bot
  materials = _materials
}

function canHarvest(block) {
  const okTools = block.harvestTools
  if (!okTools) return true
  if (bot.heldItem && okTools[bot.heldItem.type]) return true
  // see if we have the tool necessary in inventory
  const tools = bot.inventory.items().filter(item => okTools[item.type])
  const tool = tools[0]
  return !!tool
}

function toolToBreak(blockToBreak) {
  if (!canHarvest(blockToBreak)) {
    logger.error("I don't have the tool to break " + blockToBreak.name)
    return null
  }
  const material = blockToBreak.material
  if (!material) return true
  const toolMultipliers = materials[material]
  assert.ok(toolMultipliers)
  const tools = bot.inventory.items().filter(item => toolMultipliers[item.type] != null)
  tools.sort((a, b) => toolMultipliers[b.type] - toolMultipliers[a.type])
  const tool = tools[0]
  if (!tool) return true
  if (bot.heldItem && bot.heldItem.type === tool.type) return true
  return tool
}

function itemToBuild() {
  // return true if we're already good to go
  if (bot.heldItem && scaffoldBlockTypes[bot.heldItem.type]) return true
  const scaffoldingItems = bot.inventory.items().filter(item => scaffoldBlockTypes[item.type])
  const item = scaffoldingItems[0]
  if (!item) {
    logger.error("I don't have any block to build")
    return null
  }
  return item
}

function numberOfOwnedItems(name) {
  const items = bot.inventory.items()
  let c = 0
  for (const i in items) { if (items[i].name === name) c += items[i].count }
  return c
}

function myItems() {
  const items = {}
  bot.inventory.items().forEach(item => {
    if (items[item.name] === undefined) items[item.name] = 0
    items[item.name] += item.count
  })
  const nitems = []
  for (const i in items) { nitems.push([ i, items[i] ]) }

  return nitems
}

module.exports = {
  toolToBreak,
  itemToBuild,
  numberOfOwnedItems,
  myItems,
  init
}
