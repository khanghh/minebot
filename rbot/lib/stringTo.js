const Vec3 = require('vec3').Vec3

let bot, inventory, nearest, isEmpty, isNotEmpty, isWindowOpened, blocks, async

function init(_bot, _inventory, _nearest, _isEmpty, _isNotEmpty, _isWindowOpened, _blocks, _async) {
  bot = _bot
  inventory = _inventory
  nearest = _nearest
  isEmpty = _isEmpty
  isNotEmpty = _isNotEmpty
  isWindowOpened = _isWindowOpened
  blocks = _blocks
  async = _async
}

function noconv(s, u, head, done) {
  done(null, s)
}
const convs = {
  'item': function(s, u, head, done) { done(null, stringToItem(s)) },
  'position': stringToPosition,
  'entity': function(s, u, head, done) { done(null, stringToEntity(s, u)) },
  'block': function(s, u, head, done) { stringToBlock(s, done) },
  'int': function(s, u, head, done) { done(null, parseInt(s)) },
  'condition': function(s, u, head, done) { stringToPredicate(s, u, done) },
  'simpleItem': noconv,
  'simplePlayer': noconv,
  'destination': noconv,
  'simpleBlock': noconv,
  'message': noconv,
  'exp': noconv,
  'taskList': noconv,
  'blockName': noconv,
  'coordinate': noconv
}

function stringTo(pars, head, u, done) {
  async.map(pars, function(par, done) {
    if (convs[par[0]] === undefined) done(par[0] + ' not a type')
    else {
      convs[par[0]].apply(this, [ par[1], u, head, done ])
    }
  }, done)
}

function stringToBlock(s, done) {
  let v
  if ((v = new RegExp('^nearest block (.+)$').exec(s)) != null) {
    nearest.nearestBlock(v[1], done); return
  } else if ((v = new RegExp('^"(.+)"$').exec(s)) != null) {
    nearest.haveSign(v[1], done); return
  }
  done(`Cannot find block "${ s }"`)
}

function entitiesToArray() {
  const a = []
  for (const i in bot.entities) a.push(bot.entities[i])
  return a
}

function objects(name) {
  return entitiesToArray().filter(entity => entity.type === 'object' && (name === '*' || entity.objectType === name))
}

function mobs(name) {
  return entitiesToArray().filter(entity => entity.type === 'mob' && (name === '*' || entity.mobType === name))
}

function stringToEntity(s, u) {
  let v
  if (s === 'me') s = 'player ' + u
  if (s === 'bot') s = 'player ' + bot.username
  if ((v = new RegExp('^player (.+)$').exec(s)) != null) {
    if (bot.players[v[1]] === undefined) return null
    return bot.players[v[1]].entity
  }
  if ((v = new RegExp('^nearest mob (.+)$').exec(s)) != null) return nearest.nearestEntity(mobs(v[1]))
  if ((v = new RegExp('^nearest object (.+)$').exec(s)) != null) return nearest.nearestEntity(objects(v[1]))
  if ((v = new RegExp('^nearest reachable mob (.+)$').exec(s)) != null) return nearest.nearestReachableEntity(mobs(v[1]))
  if ((v = new RegExp('^nearest visible mob (.+)$').exec(s)) != null) return nearest.nearestVisibleEntity(mobs(v[1]))
  if ((v = new RegExp('^nearest reachable object (.+)$').exec(s)) != null) return nearest.nearestReachableEntity(objects(v[1]))
  return null
}

function adapted(entity) {
  const distance = bot.entity.position.distanceTo(entity.position)
  // 	const heightAdjust = entity.height*0.8  - (distance * 0.13)+distance*distance*0.008-distance*distance*distance*0.00003//*+(entity.position.y-bot.entity.position.y)*0.07*/;
  const heightAdjust = entity.height * 0.8 + distance * (0.14 + Math.random() / 30)
  return entity.position.offset(0, heightAdjust, 0)
}

function stringToAbsolutePosition(s, u, head, done) {
  let v
  if ((v = new RegExp('^adapted (.+)$').exec(s)) != null) {
    const entity = stringToEntity(v[1], u)
    if (entity != null) done(null, adapted(entity))
    else done(`Cannot find entity ${ s }`)
  } else {
    stringToBlock(s, (err, b) => {
      if (!err) done(null, b.position)
      else {
        let e
        if ((e = stringToEntity(s, u)) != null) {
          done(null, head != null ? e.position.offset(0, e.height, 0) : e.position); return
        }
        done(null, simpleStringToPosition(s))
      }
    })
  }
}

function stringToPosition(s, u, head, done) {
  let v
  if ((v = new RegExp('^nearest reachable position (.+)$').exec(s)) != null) {
    stringToPosition(v[1], u, head, (err, pos) => {
      if (!err) done(null, nearest.nearestReachablePosition(pos))
      else done(err)
    })
  } else if ((v = new RegExp('^r(.+?)\\+(.+)$').exec(s)) != null) {
    stringToAbsolutePosition(v[2], u, head, (err, pos) => {
      if (!err) done(null, pos.floored().plus(simpleStringToPosition(v[1])))
      else done(err)
    })
  } else if ((v = new RegExp('^r(.+)$').exec(s)) != null) {
    const ret = bot.entity.position.floored().plus(simpleStringToPosition(v[1]))
    done(null, ret)
  } else stringToAbsolutePosition(s, u, head, done)
}

function simpleStringToPosition(s) {
  const c = s.split(',')
  const x = parseFloat(c[0])
  const y = parseFloat(c[1])
  const z = parseFloat(c[2])
  if (!isNaN(x) && !isNaN(y) && !isNaN(z)) return new Vec3(x, y, z)
  return null
}

function itemByName(name) {
  let item, i
  for (i = 0; i < bot.inventory.slots.length; ++i) {
    item = bot.inventory.slots[i]
    if (item && item.name === name) return item
  }
  return null
}

function stringToItem(s) {
  let v
  if (new RegExp('^item to build$').exec(s) != null) return inventory.itemToBuild()
  if ((v = new RegExp('^tool to break (.+)$').exec(s)) != null) return inventory.toolToBreak(blocks[v[1]])
  return itemByName(s)
}

function stringToPredicate(c, u, done) {
  const preds = {
    'at': function(pos) {
      done(null, () => nearest.sameBlock(pos, bot.entity.position))
    },
    'have': function(count, name) {
      done(null, () => inventory.numberOfOwnedItems(name) >= count)
    },
    'close of': function(blockName) {
      done(null, () => nearest.closeOf(blockName))
    },
    'is empty': function(pos) {
      done(null, () => isEmpty(pos))
    },
    'is not empty': function(pos) {
      done(null, () => isNotEmpty(pos))
    },
    'is window opened': function() {
      done(null, () => isWindowOpened())
    },
    '>': function(v1, v2) {
      done(null, () => bot.entity.position.floored()[v1] > v2)
    },
    '<': function(v1, v2) {
      done(null, () => bot.entity.position.floored()[v1] < v2)
    },
    '=': function(v1, v2) {
      done(null, () => bot.entity.position.floored()[v1] == v2)
    }
  }
  stringTo(c[1], 2, u, function(err, pars) {
    if (err) return done('Cannot parse parameter.')
    if (c[0] in preds) preds[c[0]].apply(this, pars)
    else done(`Not understand condition "${ c[0] }".`)
  })
}

module.exports = {
  init,
  stringTo,
  stringToBlock,
  stringToEntity,
  stringToAbsolutePosition,
  simpleStringToPosition,
  stringToPosition,
  stringToItem,
  stringToPredicate
}
