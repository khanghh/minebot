const ce = require('cloneextend')
const parser = require('./grammar/grammar').parser
const logger = require('./utils/logger')
const config = require('./config')

let bot, tasks, parameterized_alias, alias, master, stringTo, giveUser

function init(_tasks, _giveUser, _parameterized_alias, _alias, _stringTo, _bot, _master) { // ou passer simplement task...
  master = _master
  bot = _bot
  tasks = _tasks
  parameterized_alias = _parameterized_alias
  alias = _alias
  stringTo = _stringTo
  giveUser = _giveUser
}

function parsedTaskToString(parsedTask) {
  function arrayToString(a) {
    return '[' + a.map(taskToString).join() + ']'
  }

  function taskToString(t) {
    if (t.constructor === Array) return arrayToString(t)
    if (t.constructor === String) return '"' + t + '"'
  }

  return taskToString(parsedTask)
}

function applyAction(task, username, parsedTask, done) {
  stringTo.stringTo(task.p, parsedTask[0] === 'look at' ? 1 : null, username, function(err, pars) {
    if (!err) task.f.apply(this, pars.concat(giveUser.indexOf(parsedTask[0]) !== -1 ? [ username ] : []).concat([ done ]))
    else done(err)
  })
}

function nameToTask(parsedTask, username, done) {
  replaceParameterizedAlias(parsedTask, username, parsedTask => {
    let task
    let pars
    if (parsedTask[0] in tasks) {
      pars = ce.clone(parsedTask[1])
      task = {
        f: ce.clone(tasks[parsedTask[0]]),
        p: pars
      }
      done(task, parsedTask)
    } else done(null, parsedTask)
  })
}

function achieve(parsedTask, username, done) {
  try {
    nameToTask(parsedTask, username, (task, parsedTask) => {
      if (task === null) {
        done('Cannot find ' + parsedTaskToString(parsedTask))
      } else {
        logger.info("I'm going to achieve task " + parsedTaskToString(parsedTask))
        setImmediate(() => {
          applyAction(task, username, parsedTask, done)
        })
      }
    })
  } catch (error) {
    done(error.stack)
  }
}

function listAux(taskNameList, i, username, done) {
  if (i < taskNameList.length) {
    setImmediate(() => {
      achieve(taskNameList[i][1], username, (function(taskNameList, i, username, done) {
        return function() {
          setImmediate(() => {
            listAux(taskNameList, i + 1, username, done)
          })
        }
      })(taskNameList, i, username, done))
    })
  } else setImmediate(done)
}

function achieveList(taskNameList, username, done) {
  listAux(taskNameList, 0, username, done)
}

// reecriture (systeme suppose confluent et fortement terminal)
function replaceAlias(message) {
  let changed = 1
  while (changed) {
    changed = 0
    for (const alia in alias) {
      const newM = message.replace(alia, alias[alia])
      if (newM !== message) {
        message = newM
        changed = 1
      }
    }
  }
  return message
}

function replaceParameterizedAlias(parsedMessage, username, done) {
  if (parsedMessage[0] in parameterized_alias) {
    let pars = ce.clone(parsedMessage[1]) // how can I use stringTo ? (removing parameterized alias ?) seems like I don't want to use it
    pars = pars.map(par => par[1])
    pars.push(username)
    pars.push(replaced => {
      replaceParameterizedAlias(parse(replaced), username, done)
    })
    parameterized_alias[parsedMessage[0]].apply(this, pars)
  } else done(parsedMessage)
}

function parse(message) {
  return parser.parse(replaceAlias(message))
}

function processMessage(message, username, done) {
  let parsedMessage
  try {
    parsedMessage = parse(message)
  } catch (error) {
    done('Syntax error')
    logger.error(error.stack)
    return
  }
  if (parsedMessage[0] in tasks || parsedMessage[0] in parameterized_alias) {
    achieve(parsedMessage, username, done)
  } else done('Unknown command.')
}

module.exports = {
  init,
  processMessage,
  achieve,
  achieveList
}
