const Vec3 = require('vec3').Vec3

let bot
let achieve
let achieveList
let blocks
let items
let blocksByName
let itemsByName
let processMessage
let inventory
let stringTo
let nearest
let syntaxTask
let moveTask
let inventoryTask
let chestTask
let farmTask
let blockTask
let informationTask
let tasks
let alias
let parameterized_alias
let giveUser
let async
const all_task = {}
let materials
let Recipe

function init(_bot, _achieve, _achieveList, _processMessage, _async) {
  bot = _bot
  const mcData = require('minecraft-data')(bot.version)
  async = _async
  processMessage = _processMessage
  achieve = _achieve
  achieveList = _achieveList
  bot.on('blockUpdate', (oldBlock, newBlock) => {
    if (newBlock != null) {
      if (isBlockEmpty(newBlock)) bot.emit('pos_' + newBlock.position + '_empty')
      else bot.emit('pos_' + newBlock.position + '_not_empty')
    }

  })
  bot.navigate.on('stop', () => {
    bot.emit('stop')
  })
  bot.navigate.on('cannotFind', () => {
    bot.emit('stop')
  })

  blocks = mcData.blocks
  items = mcData.items
  blocksByName = mcData.blocksByName
  itemsByName = mcData.itemsByName
  materials = mcData.materials
  Recipe = require('prismarine-recipe')(bot.version).Recipe
  inventory = require('./lib/inventory')
  nearest = require('./lib/nearest')
  stringTo = require('./lib/stringTo')
  inventory.init(bot, materials)
  nearest.init(bot, isNotEmpty, blocksByName)
  stringTo.init(bot, inventory, nearest, isEmpty, isNotEmpty, isWindowOpened, blocksByName, async)

  syntaxTask = require('./task/syntaxTask')
  syntaxTask.init(achieve, achieveList, stringTo)
  blockTask = require('./task/blockTask')
  blockTask.init(bot, stringTo, isNotEmpty, isBlockEmpty, isBlockNotEmpty, isEmpty, positionToString, processMessage)
  moveTask = require('./task/moveTask')
  moveTask.init(bot, processMessage, isEmpty, stringTo, isNotEmpty)
  inventoryTask = require('./task/inventoryTask')
  inventoryTask.init(bot, stringTo, findItemType, isWindowOpened, inventory)
  chestTask = require('./task/chestTask')
  chestTask.init(bot, stringTo, findItemType, isWindowOpened, inventory)
  farmTask = require('./task/farmTask')
  farmTask.init(bot, stringTo, blocksByName, findItemType, moveTask, blockTask, inventoryTask)
  informationTask = require('./task/informationTask')
  informationTask.init(bot, stringTo)
  giveUser = [ 'ifThenElse', 'ifThen', 'repeatUntil', 'repeat', 'taskList', 'replicate' ]
  tasks = {
    'ifThenElse': syntaxTask.ifThenElse,
    'ifThen': syntaxTask.ifThen,
    'repeatUntil': syntaxTask.repeatUntil,
    'repeat': syntaxTask.repeat,
    'stopRepeat': syntaxTask.stopRepeat,
    'taskList': syntaxTask.achieveListAux,
    'wait': syntaxTask.wait,
    'nothing': syntaxTask.nothing,

    'move to': moveTask.moveTo,
    'move': moveTask.move,
    'reach to': moveTask.reachTo,
    'stop move to': moveTask.stopMoveTo,
    'jump': moveTask.jump,
    'up': moveTask.up,
    'tcc': moveTask.tcc,
    // 		"avancer":avancer,c:conditionAvancer,

    'list': inventoryTask.listInventory,
    'tool': inventoryTask.tool,
    'click': inventoryTask.click,
    'toss': inventoryTask.toss,
    'equip': inventoryTask.equip,
    'unequip': inventoryTask.unequip,
    'activate item': inventoryTask.activateItem,
    'deactivate item': inventoryTask.deactivateItem,
    'craft': inventoryTask.craft,
    'close window': inventoryTask.closeWindow,

    'harvest': farmTask.harvest,
    'plant': farmTask.plant,

    'loot': chestTask.loot,
    'store': chestTask.store,

    'dig': blockTask.dig,
    'build': blockTask.build,
    'scaffold': blockTask.scaffold,
    'watch': blockTask.watch,
    'stop watch': blockTask.stopWatch,
    'replicate': blockTask.replicate,

    'pos': informationTask.pos,
    'look for block': informationTask.lookForBlock,
    'look for entity': informationTask.lookForEntity,
    'say': informationTask.say,

    'swing arm': swingArm,
    'attack': attack,
    'look at': lookAt

    // 			"achieve":achieveCondition
  }
  // ou passer à du pur string ? (what ?)
  alias = {
    'x+': 'move r1,0,0',
    'x-': 'move r-1,0,0',
    'y+': 'move r0,1,0',
    'y-': 'move r0,-1,0',
    'z+': 'move r0,0,1',
    'z-': 'move r0,0,-1',

    'attack everymob': 'repeat do move to nearest reachable mob * then attack nearest reachable mob * done done',
    'come': 'move to me'
    // 'down': 'do tcc then sbuild r0,-2,0 then sdig r0,-1,0 then wait 400 done', // could change the wait 400 to something like a when at r0,-1,0 or something
    // 'sup': 'do tcc then sdig r0,2,0 then equip hand item to build then up done'
  }

  const gss = { 'stonebrick': 'stone',
    'coal': 'oreCoal',
    'ingotIron': 'oreIron',
    'diamond': 'oreDiamond' }

  // should I put these aliases somewhere else ?
  parameterized_alias = {
    'trade recevice': function(u, done) {
      done('do say /trade ' + u + '. then wait 10000 then if is window opened then click 27 else close window endif done')
    },
    'giveEverything': function(p, u, done) {
      done('do look at ' + p + ' then toss everything done')
    },
    'give': function(p, n, i, u, done) {
      done('do look at ' + p + ' then toss ' + n + ' ' + i + ' done')
    },
    'toss everything': function(u, done) {
      const l = inventory.myItems().map(a => 'toss ' + a[1] + ' ' + a[0])
        .join(' then ')
      done(l === '' ? 'nothing' : 'do ' + l + ' done')
    },
    'sbuild': function(s, u, done) {
      done('if is empty ' + s + ' then do equip hand item to build then build ' + s + ' done endif')
    },
    'shoot': function(s, u, done) {
      done('do look at adapted ' + s + ' then activate item then wait 1000 then deactivate item done')
    },
    'get': function(s, u, done) {
      done('do move to nearest reachable position nearest block ' + s + ' then sdig nearest block ' + s + ' done')// add+" then move to nearest reachable object" when improved
    }, // do move to nearest reachable position block nearest block log then dig block nearest block log done
    'follow': function(s, u, done) {
      done('repeat do reach to ' + s + ' then wait 2000 done done')// can make it follow with some distance maybe ?
    }
  }
  all_task.tasks = tasks
  all_task.giveUser = giveUser
  all_task.alias = alias
  all_task.parameterized_alias = parameterized_alias
  all_task.stringTo = stringTo
}

function isEmpty(pos) {
  return isBlockEmpty(bot.blockAt(pos))
}

function isBlockEmpty(b) {
  return b !== null && b.boundingBox === 'empty'
}

function isWindowOpened() {
  return bot.currentWindow != null
}

function isNotEmpty(pos) {
  return isBlockNotEmpty(bot.blockAt(pos))
}

function isBlockNotEmpty(b) {
  return b !== null && b.boundingBox !== 'empty'
}

function findItemType(name) {
  let id
  if ((id = itemsByName[name]) !== undefined) return id
  if ((id = blocksByName[name]) !== undefined) return id
  return null
}

function lookAt(goalPosition, done) {
  if (goalPosition != null) { bot.lookAt(goalPosition, true) }

  done()
}

function attack(ent, done) {
  if (ent != null) bot.attack(ent)
  done()
}

function swingArm(done) {
  bot.swingArm()
  done()
}

function positionToString(p) {
  return p.x + ',' + p.y + ',' + p.z
}

module.exports = {
  all_task,
  init
}
