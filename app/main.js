import 'bootstrap'
import $ from 'jquery'
import { FitAddon } from 'xterm-addon-fit'
import { Terminal } from 'xterm'
import io from 'socket.io-client'

const listBot = {}
const currentTerm = null

function toastError(msg, title) {
  if (!title) title = 'Có lỗ i xảy ra'
  $('#toastTitle').text(title)
  $('#toastMsg').text(msg)
  $('.toast').toast({
    animation: true,
    autohide: true,
    delay: 5000
  })
    .toast('show')
}

function toastMessage(msg, title) {
  if (!title) title = 'Thông báo'
  toastError(msg, title)
}

$(window).on('resize', () => {
  for (const username in listBot) {
    const bot = listBot[username]
    bot.term.fitAddon.fit()
  }
})

$('#botList a').on('shown.bs.tab', e => {
  const username = $(e.target).data('username')
  const term = listBot[username].term
  term.focus()
  term.fitAddon.fit()
})

const socket = io('http://localhost:3000')

socket.on('data', data => {
  const username = data.username
  const bot = listBot[username]
  bot.term.write(data.data)
})

socket.on('welcome', data => {
  toastMessage(data.message, data.title)
})

socket.on('message', data => {
  toastMessage(data.message, data.title)
})

socket.on('err_message', data => {
  toastError(data.message, data.title)
})

socket.on('list', data => {
  for (const [ i, bot ] of data.botList.entries()) {
    const el = document.getElementById(`bot-${ i }`)
    const term = new Terminal({ cursorBlink: true, convertEol: true })
    const fitAddon = new FitAddon()
    term.loadAddon(fitAddon)
    term.open(el)
    term.fitAddon = fitAddon
    fitAddon.fit()
    term.prompt = () => term.write('\r\n> ')
    term.write('> ')

    listBot[bot.username] = { bot, term }
    let cmd = ''
    const cmdHistory = {}
    let cmdOffset = 0
    let cmdCount = 0
    term.attachCustomKeyEventHandler(ev => {
      if (ev.type == 'keydown') {
        if (ev.keyCode == 38) {
          if (cmdOffset > 0) {
            while (cmd) {
              term.write('\b \b')
              cmd = cmd.slice(0, -1)
            }
            cmdOffset -= 1
            cmd = cmdHistory[cmdOffset]
            term.write(cmd)
          }
          return false
        } else if (ev.keyCode == 40) {
          if (cmdOffset + 1 <= cmdCount) {
            while (cmd) {
              term.write('\b \b')
              cmd = cmd.slice(0, -1)
            }
            cmdOffset += 1
            cmd = cmdHistory[cmdOffset]
            term.write(cmd)
          }
          return false
        }
      }
      return true
    })

    term.onData(text => {
      if (/^[a-zA-Z0-9!"#$%&'()*+,.\/:;<=>?@\[\] ^_`{|}~-]*$/.test(text)) {
        term.write(text)
        cmd += text
      }
    })

    term.onKey(e => {
      const ev = e.domEvent
      if (ev.keyCode === 13) {
        if (cmd == 'clear') term.clear()
        else if (cmd) {
          cmdHistory[cmdCount] = cmd
          cmdCount += 1
          cmdHistory[cmdCount] = ''
          cmdOffset = cmdCount
          socket.emit('cmd', { username: bot.username, cmd })
        }
        term.prompt()
        cmd = ''
      } else if (ev.keyCode === 8) {
        if (cmd.length > 0) {
          term.write('\b \b')
          cmd = cmd.slice(0, -1)
        }
      }
    })
  }
  $('#botList a[href="#bot-0"]').tab('show')
})

socket.on('disconnect', () => {
  toastError('Mất kết nối.')
})
