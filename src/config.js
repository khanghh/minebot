module.exports = {
  port: process.env.port,
  cookieSecret: process.env.cookieSecret,
  password: process.env.password,
  userList: process.env.userList ? process.env.userList.split(',') : 'admin',
  node: process.env.node
}
