const winston = require('winston')

const createLogger = function (objectName, logFile = undefined) {
  const myFormat = winston.format.printf(
    info =>
      winston.format
        .colorize()
        .colorize(info.level, `[${ info.timestamp }][${ objectName }][${ info.level.toUpperCase() }]: `) + info.message
  )

  const myFormatFile = winston.format.printf(
    info => (info.level, `[${ info.timestamp }][${ objectName }][${ info.level.toUpperCase() }]: `) + info.message
  )

  return winston.createLogger({
    format: winston.format.timestamp(),
    transports: [
      new winston.transports.Console({
        format: myFormat
      })
    ]
  })
}

module.exports = createLogger
