const { spawn } = require('child_process')
const config = require('./config')
const path = require('path')
const logger = require('./utils/createLogger')('BotMgr')

class BotMgr {
  constructor(serverio) {
    this.serverio = serverio
    this.botList = {}
    const listBot = require('./bot.json')
    for (const bot of listBot) { this.botList[bot.username] = bot }
  }

  startBot(username) {
    const bot = this.botList[username]
    const env = {
      username: bot.username,
      password: bot.password,
      host: bot.host,
      port: bot.port,
      owners: bot.owners.join(',')
    }
    const rbot = path.resolve(__dirname, '../rbot/rbot.js')
    const proc = spawn(config.node, [ rbot, '--color=always' ], { env })
    bot.proc = proc
    bot.proc.isrunning = true
    proc.stdout.on('data', data => {
      const text = data.toString().replace(/\n/g, '\r\n')
      bot.output += text
      this.serverio.sockets.emit('data', { username: username, data: text })
    })
    proc.stderr.on('data', data => {
      logger.info(data.toString())
    })
    proc.on('close', code => {
      proc.isrunning = false
      bot.output = null
      this.serverio.sockets.emit('message', { username: username, message: `Bot ${ username } đã thoát. Code: ${ code }` })
    })
    proc.on('error', err => {
      logger.error(err)
      // self.restartBot(username)
      this.serverio.sockets.emit('err_message', { username: username, message: err })
    })
  }

  restartBot(username) {
    const bot = this.botList[username]
    if (bot.proc) { bot.proc.kill('SIGINT') }
    this.startBot(username)
  }

  stopBot(username) {
    const bot = this.botList[username]
    if (bot.proc) { bot.proc.kill('SIGINT') }
  }

  sendBotCmd(owner, username, cmd, cb) {
    const bot = this.botList[username]
    if (!bot.owners.includes(owner)) {
      cb(username, 'Bạn không có quyền sử dụng bot này.')
      return
    }
    logger.info(`${ owner }:${ username }:${ cmd }`)
    if (bot) {
      if (cmd === 'start') {
        if (!bot.proc || !bot.proc.isrunning) { this.startBot(username) }
        cb('Bot đã online.')
      } else if (cmd === 'restart') {
        this.restartBot(username)
        cb('Đã khởi động lại bot.')
      } else if (cmd === 'stop') {
        this.stopBot(username)
        cb(`Bot ${ username } đã thoát.`)
      } else if (bot.proc && bot.proc.isrunning) {
        cmd = `${ owner }:${ cmd }`
        bot.proc.stdin.write(cmd)
      } else {
        cb('Bot không online.')
      }
    } else { cb('Không tìm thấy bot trong danh sách.') }
  }

  addBot(bot) {
    this.botList[bot.username] = bot
  }

  getBot(username) {
    if (username in this.botList) { return this.botList[username] }
    return null
  }

}

module.exports = BotMgr
