const express = require('express')
const socketio = require('socket.io')
const bodyParser = require('body-parser')
const cookieSession = require('cookie-session')
const http = require('http')
const path = require('path')
const handleSocket = require('./socket/handleSocket')

const config = require('./config')
const logger = require('./utils/createLogger')('main')

const session = cookieSession({
  name: 'bot',
  secret: config.cookieSecret,
  signed: true,
  maxAge: 2 * 60 * 60 * 1000 // 24 hours
})

const app = express()
app.use(session)
app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, '/views'))
app.use('/assets', express.static(path.join(__dirname, '/assets')))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

const auth_required = (req, res, next) => {
  if (!req.session.username)
    return res.redirect('/login')
  next()
}

app.get('/', auth_required, (req, res) => {
  const botList = require('./bot.json')
  res.render('index', { username: req.session.username, botList })
})

app.get('/login', (req, res) => {
  if (req.session.username)
    return res.redirect('/')
  res.render('login')
})

app.post('/login', (req, res) => {
  const username = req.body.username
  const password = req.body.password
  if (password != config.password || !config.userList.includes(username))
    res.render('login', { error: 'Tài khoản hoặc mật khẩu không chính xác.' })
  else {
    req.session.username = username
    res.redirect('/')
  }
})

const server = http.createServer(app)
const serverio = socketio().listen(server)

const PORT = config.port || 3000
server.listen(PORT, () => {
  logger.info('Listening on port ' + server.address().port)
  handleSocket(serverio, session)
})
