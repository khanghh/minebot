#!/usr/bin/env node
const mineflayer = require('mineflayer')
const blockFinderPlugin = require('mineflayer-blockfinder')(mineflayer)
const navigatePlugin = require('mineflayer-navigate')(mineflayer)
const navigate2Plugin = require('./avoidBedrock.js')(mineflayer)
const scaffoldPlugin = require('mineflayer-scaffold')(mineflayer)
const async = require('async')
const chalk = require('chalk')
const config = require('./config')
const logger = require('./utils/logger')

const bot = mineflayer.createBot(config.botConfig)

function sleep(duration) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve()
    }, duration)
  })
}

navigatePlugin(bot)
navigate2Plugin(bot)
scaffoldPlugin(bot)
bot.loadPlugin(blockFinderPlugin)
const task = require('./task')
const achieve = require('./achieve')

bot.loadPlugin(() => {
  task.init(bot, achieve.achieve, achieve.achieveList, achieve.processMessage, async)
  achieve.init(task.all_task.tasks, task.all_task.giveUser, task.all_task.parameterized_alias, task.all_task.alias, task.all_task.stringTo, bot)
})

bot.on('error', err => {
  logger.error(err)
})

bot.on('login', () => {
})

bot.on('windowOpen', window => {
  logger.info('==================== window open =====================')

  logger.info('=================== end window open ===================')
})

bot.on('windowClose', window => {
  logger.info('Window closed')
})

bot.on('spawn', () => { })

bot.on('death', () => { })

bot.on('chat', (username, message) => {
  if (username != config.botConfig.username && config.owners.includes(username)) {
    logger.command(username, message)
    achieve.processMessage(message, username, err => {
      if (err) bot.chat(`I failed task "${ message }". ${ err }`)
      else bot.chat(`I achieved task "${ message }."`)
    })
  }
})

bot.on('whisper', (username, message) => {
})

bot.on('message', msg => {
  msg = String(msg)
  logger.chat(chalk.gray(msg))
  if (msg.indexOf('/tpaccept.') != -1) { bot.chat('/tpaccept') }

  if (msg == 'Đăng nhập thành công!') {
    sleep(1000).then(() => {
      bot.setQuickBarSlot(4)
      return sleep(1000)
    })
      .then(() => {
        bot.activateItem()
      })
  }
})

bot.navigate.on('pathFound', path => {
  logger.info('found path. I can get there in ' + path.length + ' moves.')
})
bot.navigate.on('cannotFind', () => {
  logger.info('unable to find path')
})

bot.on('health', () => {
  logger.info('hp:' + bot.health + ', food:' + bot.food)
})

bot.on('playerJoined', player => {
  // console.log("hello, " + player.username + "! welcome to the server.");
})
bot.on('playerLeft', player => {
  // console.log("bye " + player.username);
})
bot.on('kicked', reason => {
  logger.error('I got kicked for ', reason)
})

bot.on('nonSpokenChat', message => {
  logger.info('non spoken chat', message)
})
