const config = require('../config')
const BotMgr = require('../BotMgr')

const logger = require('../utils/createLogger')('SocketHandler')

module.exports = (serverio, session) => {

  const botList = require('../bot.json')
  const botMgr = new BotMgr(serverio, botList)

  serverio.on('connection', socket => {
    const req = {
      connection: { encrypted: false },
      headers: { cookie: socket.request.headers.cookie }
    }
    const res = {
      getHeader: () => {},
      setHeader: () => {}
    }
    session(req, res, () => {
      if (req.session.username) {
        socket.username = req.session.username
        socket.emit('welcome', { title: 'Xin chào', message: `Chào mừng ${ req.session.username } đã quay lại!` })
        const tmpList = []
        for (const bot of botList) {
          tmpList.push({
            username: bot.username,
            owners: bot.owners,
            host: bot.host,
            port: bot.port
          })
        }
        socket.emit('list', { botList: tmpList })
        for (const username in botMgr.botList) {
          const bot = botMgr.botList[username]
          if (bot.output) { socket.emit('data', { username: username, data: bot.output }) }
        }
        logger.info(`${ socket.username } connected!`)
      }
    })

    socket.on('cmd', data => {
      const username = data.username
      const cmd = data.cmd
      const owner = socket.username
      botMgr.sendBotCmd(owner, username, cmd, message => {
        socket.emit('message', { username, message })
      })
    })

  })

}
