const colors = [ '§0', '§1', '§2', '§3', '§4', '§5', '§6', '§7', '§8', '§9', '§a', '§b', '§c', '§d', '§e', '§f', '§k', '§l', '§m', '§n', '§o', '§r' ]
module.exports = function(str) {
  str = String(str)
  for (const color of colors)
    str = str.replace(color, '')
  return str
}
