const Vec3 = require('vec3').Vec3

let bot, processMessage, isEmpty, isNotEmpty, stringTo, directions, direction

function init(_bot, _processMessage, _isEmpty, _stringTo, _isNotEmpty) {
  bot = _bot
  processMessage = _processMessage
  isEmpty = _isEmpty
  isNotEmpty = _isNotEmpty
  stringTo = _stringTo
  bot.navigate.blocksToAvoid = {
    51: true, // fire
    // 59: true, // crops
    10: true, // lava
    11: true // lava
  }
}

function jump(done) {
  bot.setControlState('jump', true)
  bot.setControlState('jump', false)
  setTimeout(done, 400)// change this
}

function up(done) { // change this a bit ?
  if (isNotEmpty(bot.entity.position.offset(0, 2, 0))) {
    done(true); return
  }
  // if(bot.heldItem===null) {done(true);return;} // replace this with something checking whether the bot has a block to build ?
  const targetBlock = bot.blockAt(bot.entity.position.offset(0, -1, 0))
  const jumpY = bot.entity.position.y + 1
  bot.setControlState('jump', true)
  // bot.on('move', placeIfHighEnough);
  setTimeout(place, 400)

  function placeIfHighEnough() {
    if (bot.entity.position.y > jumpY) { place() }

  }

  function place() {
    bot.placeBlock(targetBlock, new Vec3(0, 1, 0), () => {
      setTimeout(done, 400)// could (should ?) be replaced my something checking whether the bot is low enough/has stopped moving
    })
    // dirty
    // processMessage(u,"sbuild r0,-1,0",function(){ // this is very wrong, solve it somehow (doesn't take into account the parameter of the callback as in achieve)
    bot.setControlState('jump', false)
    bot.removeListener('move', placeIfHighEnough)
    // });
  }
}

function center(p) {
  p.y = Math.round(p.y)
  return p.floored().offset(0.5, 0, 0.5)
}

function scalarProduct(v1, v2) {
  return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z
}

function norm(v) {
  return Math.sqrt(scalarProduct(v, v))
}

function isFree(pos) {
  return isEmpty(pos) && isEmpty(pos.offset(0, 1, 0))
}

function move(goalPosition, done) {
  goalPosition = center(goalPosition)
  const canArrive = isFree(goalPosition)
  bot.lookAt(goalPosition.offset(0, bot.entity.height, 0), true)
  bot.setControlState('forward', true)
  const arrive = setInterval(() => {
    const d = goalPosition.distanceTo(bot.entity.position)
    if (d < 0.3 || d < 1.3 && !canArrive) {
      bot.setControlState('forward', false)
      clearInterval(arrive)
      if (canArrive) done()
      else done(`I arrive but cannot reach the position ${ goalPosition }`)
    } else bot.lookAt(goalPosition.offset(0, bot.entity.height, 0), true)
  }, 50)
}

function moveTo(goalPosition, done) {
  if (goalPosition) {
    goalPosition = center(goalPosition)
    if (goalPosition.distanceTo(bot.entity.position) >= 0.2) {
      const a = bot.navigate.findPathSync(goalPosition, { timeout: 5000 })
      if (a.path.length <= 1) done()
      else if (a.status === 'success') bot.navigate.walk(a.path, msg => msg === 'arrived' ? done() : done(msg))
      else if (a.status === 'noPath') done(`Couldn't find any path to reach the position ${ goalPosition }`)
      else if (a.status === 'tooFar') done('Too far.')
      else if (a.status === 'timeout') done('Time out')
      else done('Unknown error.')
    } else done()
  } else done('Unknown position.')
}

function reachTo(goalPosition, done) {
  if (goalPosition) {
    goalPosition = center(goalPosition)
    if (goalPosition.distanceTo(bot.entity.position) > 1) {
      const a = bot.navigate.findPathSync(goalPosition, {
        timeout: 5000,
        endRadius: 1
      })
      if (a.path.length <= 1) done()
      else if (a.status === 'success') {
        const endPoint = a.path[a.path.length - 1]
        if (endPoint.distanceTo(goalPosition) < 1) a.path.pop()
        bot.navigate.walk(a.path, msg => msg === 'arrived' ? done() : done(msg))
      } else if (a.status === 'noPath') done('No path.')
      else if (a.status === 'tooFar') done('Too far.')
      else if (a.status === 'timeout') done('Time out.')
      else done('Unknown error.')
    } else done()
  } else done('Unknown position.')
}

function stopMoveTo(done) {
  bot.navigate.stop()
  done()
}

function tcc(done) {
  bot.entity.position = center(bot.entity.position)
  setTimeout(done, 200)
}

module.exports = {
  jump,
  up,
  move,
  moveTo,
  reachTo,
  stopMoveTo,
  tcc,
  init
}
