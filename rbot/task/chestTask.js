const Vec3 = require('vec3').Vec3

let bot, stringTo, findItemType, isWindowOpened, inventory

function init(_bot, _stringTo, _findItemType, _isWindowOpened, _inventory) {
  bot = _bot
  stringTo = _stringTo
  findItemType = _findItemType
  isWindowOpened = _isWindowOpened
  inventory = _inventory
}

function loot(position, amount, itemName, done) {
  // console.log('loot1')
  if (position) {
    const block = bot.blockAt(position)
    if ([ 54, 146, 130 ].includes(block.type)) { // chest, trapped_chest, ender_chest
      // console.log('loot2')
      bot.lookAt(block.position)
      const chest = bot.openChest(block)
      chest.on('open', () => {
        // console.log('loot3')
        const item = findItemType(itemName)
        // console.log(item.id)
        // console.log(amount)
        chest.withdraw(item.id, null, amount, err => {
          chest.close()
          if (err) { done(`I cannot withdraw ${ amount } ${ itemName } from ${ position }: ${ err } `) } else done()
        })
      })
    } else done(`I cannot loot from ${ position }.It is not a chest type.`)
  } else done(`Cannot find the position.`)
}

function store(position, amount, itemName, done) {
  if (position) {
    const block = bot.blockAt(position)
    if ([ 54, 146, 130 ].includes(block.type)) { // chest, trapped_chest, ender_chest
      const chest = bot.openChest(block)
      chest.on('open', () => {
        const item = findItemType(itemName)
        chest.deposit(item.id, null, amount, err => {
          chest.close()
          if (err) { done(`I cannot deposit ${ amount } ${ itemName } to ${ position }: ${ err } `) } else done()
        })
      })
    } else done(`I cannot store to ${ position }.It is not a chest type.`)

  } else done(`Cannot find the position.`)
}

module.exports = {
  loot,
  store,
  init
}
