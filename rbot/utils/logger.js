const chalk = require('chalk')

/* eslint-disable no-console */
function info(msg) {
  console.log(chalk.green('info: ') + chalk.white(msg))
}

function chat(msg) {
  console.log(chalk.gray(msg))
}

function command(username, msg) {
  console.log(chalk.yellow(`${ username }:`) + chalk.white(msg))
}

function error(msg) {
  console.log(chalk.red('error:') + chalk.white(msg))
}

module.exports = {
  info,
  error,
  chat,
  command
}
