let stop, achieve, achieveList, stringTo

function init(_achieve, _achieveList, _stringTo) {
  achieve = _achieve
  achieveList = _achieveList
  stringTo = _stringTo
  stop = false
}

function repeatAux(taskName, over, username, done) {
  setImmediate(() => {
    achieve(taskName, username, err => {
      if (err) done(err)
      else if (!over()) repeatAux(taskName, over, username, done)
      else done()
    })
  })
}

function repeat(taskName, username, done) {
  stop = false
  repeatAux(taskName, () => stop, username, done)
}

function stopRepeat(done) {
  stop = true
  done()
}

function ifThenElse(pred, then, els, u, done) {
  if (pred()) achieve(then, u, done)
  else achieve(els, u, done)
}

function ifThen(pred, then, u, done) {
  if (pred()) achieve(then, u, done)
  else done()
}

function repeatUntil(taskName, pred, u, done) {
  stop = false
  repeatAux(taskName, () => stop || pred(), u, done)
}

function achieveListAux(p, u, done) {
  achieveList(p, u, done)
}

function nothing(done) {
  done()
}

function wait(time, done) {
  setTimeout(done, time)
}

module.exports = {
  repeat,
  stopRepeat,
  ifThen,
  ifThenElse,
  repeatUntil,
  achieveListAux,
  nothing,
  wait,
  init
}
