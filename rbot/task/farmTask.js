const Vec3 = require('vec3').Vec3
const logger = require('../utils/logger')

const grown = {
  59: 7, // wheat
  207: 3, // beetroots
  141: 7, // carrots
  142: 7, // potatoes
  91: 2, // lit_pumpkin
  103: 0, // melon_block
  127: 8 // cocoa
}

const lands = {
  295: [ 60, null ], // wheat_seeds
  435: [ 60, null ], // beetroot_seeds
  391: [ 60, null ], // carrot
  392: [ 60, null ], // potato
  361: [ 60, null ], // pumpkin_seeds
  362: [ 60, null ], // melon_seeds
  338: [ 12, null ], // reeds
  81: [ 12, null ], // cactus
  372: [ 88, null ] // nether_wart
}

let bot, stringTo, blockByName, findItemType, moveTask, blockTask, inventoryTask

function init(_bot, _stringTo, _blockByName, _findItemType, _moveTask, _blockTask, _inventoryTask) {
  bot = _bot
  stringTo = _stringTo
  blockByName = _blockByName
  findItemType = _findItemType
  moveTask = _moveTask
  blockTask = _blockTask
  inventoryTask = _inventoryTask
}

function findBlockName(name) {
  let blockInfo
  if ((blockInfo = blockByName[name]) !== undefined) return blockInfo
  return null
}

function center(p) {
  return p.floored().offset(0.5, 0, 0.5)
}

function itemByName(name) {
  let item, i
  for (i = 0; i < bot.inventory.slots.length; ++i) {
    item = bot.inventory.slots[i]
    if (item && item.name === name) return item
  }
  return null
}

function harvest(blockName, done) {
  const harvestBlock = findBlockName(blockName)
  if (harvestBlock) {
    bot.findBlock({
      point: bot.entity.position,
      matching: block => {
        if (grown[block.type] != undefined) return block.type == harvestBlock.id && block.metadata == grown[block.type]
        return block.type == harvestBlock.id
      },
      maxDistance: 20,
      count: 1
    }, (err, blocks) => {
      if (err) done(`Error trying to find ${ blockName }: ${ err }`)
      else if (blocks.length) {
        const block = blocks[0]
        const position = center(block.position)
        moveTask.reachTo(position, err => {
          if (err) done(`Cannot reach to ${ position }`)
          else {
            bot.lookAt(position, true)
            bot.dig(bot.blockAt(position), done)
          }
        })
      } else done("I couldn't find any " + blockName + ' within 20m.')
    })
  } else done(`${ blockName } is not supported in this server.`)
}

function plant(amount, seedName, done) {
  if (amount < 1) return done()
  const seedItem = itemByName(seedName)
  if (seedItem) {
    inventoryTask.equip('hand', seedItem, err => {
      if (err) return done(err)
      const land = lands[seedItem.type]
      if (land != undefined) {
        bot.findBlock({
          point: bot.entity.position,
          matching: block => {
            const blockAbove = bot.blockAt(block.position.offset(0, 1, 0))
            return block.type == land[0] && blockAbove.type === 0 // check if air is on top of farmland
          },
          maxDistance: 20,
          count: 1
        }, (err, blocks) => {
          if (err) done(`Error trying to find block ${ land[0] }: ${ err }`)
          else if (blocks.length) {
            const block = blocks[0]
            const position = block.position.offset(0, 1, 0).floored()
            moveTask.moveTo(position, err => {
              if (err) done(`Cannot reach to ${ position }: ${ err }`)
              else {
                // bot.lookAt(position)
                blockTask.build(position, err => {
                  if (err) {
                    done(`I couldn't plant ${ seedName }: ${ err } `)
                  } else setImmediate(() => plant(amount - 1, seedName, done))
                })
              }
            })
          } else done(`Cannot find any land to plant ${ seedName } within 20m.`)
        })
      } else done(`No land to plant ${ seedName }.`)
    })
  } else done(`${ seedName } is not supported in this server.`)
}

module.exports = {
  harvest,
  plant,
  init
}
