module.exports = {
  botConfig: {
    username: process.env.username,
    verbose: true,
    port: parseInt(process.env.port) || 25565,
    host: process.env.host,
    version: '1.12.2'
  },
  password: process.env.password,
  owners: process.env.owners ? process.env.owners.split(',') : [],
  users: process.env.users ? process.env.users.split(',') : []
}
