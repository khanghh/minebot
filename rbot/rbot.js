#!/usr/bin/env node
const mineflayer = require('mineflayer')
const blockFinderPlugin = require('mineflayer-blockfinder')(mineflayer)
const navigatePlugin = require('mineflayer-navigate')(mineflayer)
const navigate2Plugin = require('./avoidBedrock.js')(mineflayer)
const scaffoldPlugin = require('mineflayer-scaffold')(mineflayer)
const async = require('async')
const chalk = require('chalk')
const config = require('./config')
const stripColor = require('./utils/stripColor')
const logger = require('./utils/logger')

const bot = mineflayer.createBot(config.botConfig)

function sleep(duration) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve()
    }, duration)
  })
}

navigatePlugin(bot)
navigate2Plugin(bot)
scaffoldPlugin(bot)
bot.loadPlugin(blockFinderPlugin)
const task = require('./task')
const achieve = require('./achieve')

bot.loadPlugin(() => {
  task.init(bot, achieve.achieve, achieve.achieveList, achieve.processMessage, async)
  achieve.init(task.all_task.tasks, task.all_task.giveUser, task.all_task.parameterized_alias, task.all_task.alias, task.all_task.stringTo, bot, process.argv[6])
})

bot.on('error', err => {
  logger.error(err)
})

bot.on('login', () => {
})

bot.on('windowOpen', window => {
  logger.info('==================== window open =====================')
  logger.info(`title: ${ window.title }`)
  if (window.title.includes('Cụm Máy Chủ')) {
    sleep(1000).then(() => {
      bot.clickWindow(30, 0, 0)
    })
  } else if (window.title.includes('SERVER 1')) {
    sleep(1000).then(() => {
      bot.clickWindow(0, 0, 0)
      bot.chatPatterns = [
        {
          'pattern': new RegExp(`^\\[(.+) -> tôi\\] (.*)$`),
          'type': 'whisper',
          'description': 'Essentials whisper'
        },
        {
          'pattern': new RegExp(`^(.+): (.*)$`),
          'type': 'chat',
          'description': 'server message'
        }
      ]
    })
  }

  logger.info('=================== end window open ===================')
})

bot.on('windowClose', window => {
  logger.info('windowClose.')
})

bot.on('spawn', () => { })

bot.on('death', () => { })

bot.on('chat', (username, message) => {
  message = stripColor(message)
  logger.chat(`${ username } ✎ ${ message }`)
  if (username == 'nh' && message.startsWith('/DN')) {
    logger.info(`login ${ bot.player.username }`)
    bot.chat(`/dn ${ config.password }`)
  }
})

bot.on('whisper', (username, message) => {
  logger.chat(`${ username }: ${ message }`)
  username = username
    .replace('۞VIP۞', '')
    .replace('۞VIP2۞', '')
    .replace('۞VIP3۞', '')
    .replace('۞VIP4۞', '')
    .replace('۞VIP5۞', '')
  if (username.indexOf('[') != -1) { username = username.substring(0, username.indexOf('[')) }

  if (config.owners.includes(username)) {
    logger.command(username, message)
    achieve.processMessage(message, username, err => {
      if (err) {
        logger.info(`I failed task "${ message }". ${ err }`)
      } else logger.info(`I achieved task "${ message }."`)
    })
  }
})

bot.on('message', message => {
  message = String(message)
  message = stripColor(message)
  logger.chat(message)
  if (message.indexOf('/tpaccept.') != -1) { bot.chat('/tpaccept') }
  if (message == 'Đăng nhập thành công!') {
    sleep(1000).then(() => {
      bot.setQuickBarSlot(4)
      return sleep(1000)
    })
      .then(() => {
        bot.activateItem()
      })
  }
})

bot.navigate.on('pathFound', path => {
  logger.info('found path. I can get there in ' + path.length + ' moves.')
})
bot.navigate.on('cannotFind', () => {
  logger.info('unable to find path')
})

bot.on('health', () => {
  logger.info('hp:' + bot.health + ', food:' + bot.food)
})

bot.on('playerJoined', player => {
})
bot.on('playerLeft', player => {
})
bot.on('kicked', reason => {
  reason = stripColor(reason)
  logger.error('I got kicked for: ' + reason)
})

bot.on('nonSpokenChat', message => {
  logger.info('nonSpokenChat: ' + message)
})

const stdin = process.openStdin()
stdin.addListener('data', data => {
  let cmd = data.toString().trim()
  const offset = cmd.indexOf(':')
  let username = config.owners[0]
  if (offset > 0) {
    username = cmd.slice(0, offset)
    cmd = cmd.slice(offset + 1)
  }
  logger.command(username, cmd)
  achieve.processMessage(cmd, username, err => {
    if (err) {
      logger.info(`I failed task "${ cmd }". ${ err }`)
    } else logger.info(`I achieved task "${ cmd }."`)
  })
})
